package tests;

import static org.junit.jupiter.api.Assertions.*;

import java.time.LocalDate;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import clases.Cliente;
import clases.Factura;
import clases.GestorContabilidad;

class TestClientes {
	
	static GestorContabilidad unGestor;

	@BeforeEach
	void setUp() throws Exception {
		
		unGestor = new GestorContabilidad();
	}

	@Test 
	public void existeCliente() {
		Cliente unCliente = new Cliente("Teodoro", "23985431S", LocalDate.parse("2018-03-24"));
		
		unGestor.getListaClientes().add(unCliente);
		
		String dni = "23985431S";
		
		 Cliente clienteActual = unGestor.buscarCliente(dni);
		
		assertTrue(unGestor.getListaClientes().contains(clienteActual));
	}
	
	@Test
	public void noExisteCliente() {
		Cliente unCliente = new Cliente("Sebastian", "23987453L", LocalDate.parse("2018-01-12"));
		
		String dni = "23987453L";
		
		unGestor.buscarCliente(dni);
		
		assertFalse(unGestor.getListaClientes().contains(unCliente));
	}
	
	@Test
	public void buscarClienteInexistenteConMasClientes() {
		
		Cliente unCliente = new Cliente("Romero", "42309764T", LocalDate.parse("2016-05-24"));
		Cliente otroCliente = new Cliente("Carlos", "25034892T", LocalDate.parse("2017-08-17"));
		
		unGestor.getListaClientes().add(unCliente);
		unGestor.getListaClientes().add(otroCliente);
		
		String dni = "23017638D";
		
		Cliente clienteActual = unGestor.buscarCliente(dni);
		
		assertFalse(unGestor.getListaClientes().contains(clienteActual));
		
	}
	
	@Test
	public void buscarClienteExistenteConMasClientes() {
		Cliente unCliente2 = new Cliente("Jes�s", "09740582J", LocalDate.parse("2017-04-03"));
		Cliente otroCliente2 = new Cliente("Alizia", "75602391B", LocalDate.parse("2016-05-16"));
		Cliente otroCliente3 = new Cliente("Mario", "63928765R", LocalDate.parse("2018-01-31"));
		
		unGestor.getListaClientes().add(unCliente2);
		unGestor.getListaClientes().add(otroCliente2);
		unGestor.getListaClientes().add(otroCliente3);
		
		String dni = "09740582J";
		
		Cliente clienteActual = unGestor.buscarCliente(dni);
		
		assertTrue(unGestor.getListaClientes().contains(clienteActual));
		
	}
	
	@Test
	public void agregaCliente() {
		Cliente unCliente3 = new Cliente("Tamara", "05623482F", LocalDate.parse("2017-06-22"));
		
		unGestor.altaCliente(unCliente3);
		
		assertTrue(unGestor.getListaClientes().contains(unCliente3));
	}
	
	@Test
	public void noAgregaCliente() {
		Cliente unCliente4 = new Cliente("Elsa", "87340215S", LocalDate.parse("2017-03-24"));
		Cliente otroCliente4 = new Cliente("Jeremy", "87340215S", LocalDate.parse("2017-12-08"));
		
		unGestor.altaCliente(unCliente4);
		unGestor.altaCliente(otroCliente4);
		
		
	}
	
	@Test
	public void clienteMasAntiguoDeLaLista() {
		
		Cliente unCliente5 = new Cliente("William", "76309821T", LocalDate.parse("2016-02-02"));
		Cliente otroCliente5 = new Cliente("Teresa", "98345672P", LocalDate.parse("2017-05-12"));
		
		unGestor.getListaClientes().add(unCliente5);
		unGestor.getListaClientes().add(otroCliente5);
		
		unGestor.clienteMasAntiguo();
		
		assertEquals(unCliente5, otroCliente5);
	}
	
	@Test
	public void eliminaClienteSinFactura() {
		Cliente unCliente6 = new Cliente("Juli�n", "98234765H", LocalDate.parse("2018-01-23"));
		
		unGestor.getListaClientes().add(unCliente6);
		
		String dni = "98234765H";
		
		unGestor.eliminarCliente(dni);
		
		assertFalse(unGestor.getListaClientes().contains(unCliente6));
		
		
	}
	
	@Test
	public void eliminaClienteConFactura() {
		Cliente unCliente7 = new Cliente("Emma", "74912765E", LocalDate.parse("2015-05-23"));
		Factura unaFactura = new Factura("0813824673", LocalDate.parse("2017-03-22"), "gafas", 14.5F, 2, unCliente7);
		
		unGestor.getListaClientes().add(unCliente7);
		unGestor.getListaFacturas().add(unaFactura);
		
		String dni = "74912765E";
		
		unGestor.eliminarCliente(dni);
		
		assertTrue(unGestor.getListaClientes().contains(unCliente7));
	}
	
}
