package tests;

import static org.junit.jupiter.api.Assertions.*;

import java.time.LocalDate;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import clases.Cliente;
import clases.Factura;
import clases.GestorContabilidad;



class TestFactura {
	
	static GestorContabilidad unGestor;

	@BeforeEach
	void setUp() throws Exception {
	}

	@Test
	public void existeFactura() {
		Cliente unCliente = new Cliente("Pedro", "01352837D", LocalDate.parse("2016-08-25"));
		Factura unaFactura = new Factura("98347817237", LocalDate.parse("2013-04-22"), "gafas", 20.95F, 2, unCliente);
		
		
		unGestor.getListaClientes().add(unCliente);
		unGestor.getListaFacturas().add(unaFactura);
		
		String codigo = "98347817237";
		
		Factura facturaActual = unGestor.buscarFactura(codigo);
		
		assertTrue(unGestor.getListaFacturas().contains(facturaActual));
	}
	
	public void noExisteFactura() {
		
		Cliente unCliente = new Cliente("Carla", "01392637D", LocalDate.parse("2015-07-25"));
		Factura unaFactura = new Factura("012837329238", LocalDate.parse("2014-06-22"), "lapices", 22.95F, 6, unCliente);
		
		unGestor.getListaClientes().add(unCliente);
		
		String codigo = "012837329238";
		Factura facturaActual = unGestor.buscarFactura(codigo);
		
		assertFalse(unGestor.getListaFacturas().contains(facturaActual));
		
	}
	
	
	@Test 
	public void eliminarFacturaInexistente() {
		Cliente unCliente = new Cliente("Sofia", "01119837D", LocalDate.parse("2012-03-28"));
		Factura unaFactura = new Factura("238723947", LocalDate.parse("2015-03-21"), "boligrafos", 1.95F, 3, unCliente);		
		String codigo = "238723947";
		unGestor.eliminarFactura(codigo);
		assertFalse(unGestor.getListaFacturas().contains(unaFactura));
	}
	
	@Test 
	public void EliminarFacturaExistente() {
		
		Cliente unCliente = new Cliente("Paula", "98752337D", LocalDate.parse("2016-11-25"));
		Factura unaFactura = new Factura("129283274", LocalDate.parse("2017-05-10"), "almohadas", 31.95F, 3, unCliente);
		
		unGestor.listaFacturas.add(unaFactura);
		
		String codigo = "129283274";
		unGestor.eliminarFactura(codigo);
		
		assertTrue(unGestor.getListaFacturas().contains(unaFactura));
	}
	
	@Test
	public void AnyadirNuevaFactura() {
		Cliente unCliente = new Cliente ("Rosa", "23876543F", LocalDate.parse("2017-03-14"));
		Factura unaFactura = new Factura("43272634", LocalDate.parse("2018-02-03"), "silla", 49.95F, 3, unCliente);
		unGestor.crearFactura(unaFactura);
		assertTrue(unGestor.getListaFacturas().contains(unaFactura));
	}
	
	
	
	

}
